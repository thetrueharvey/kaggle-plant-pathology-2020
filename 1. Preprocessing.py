"""
Dataset preprocessing and EDA
"""
#%% Setup
import numpy  as np
import pandas as pd

from PIL import Image
import cv2

import matplotlib.pyplot as plt

#%% Label EDA
# Load the labels
train_labels = pd.read_csv("data/plant-pathology-2020-fgvc7/test.csv")

# Label distributions
value_counts = pd.DataFrame(dict( total  =[len(train_labels)]
                                , healthy=train_labels["healthy"].sum()
                                , multi  =train_labels["multiple_diseases"].sum()
                                , rust   =train_labels["rust"].sum()
                                , scab   =train_labels["scab"].sum()
                                )
                           )

'''
There are 1821 samples in the training data, approximately evenly split between
'healthy', 'rust' and 'multi'

We will check a classifier that is 2 output sigmoid
[0,0] = 'healthy'
[1,0] = 'rust'
[0,1] = 'scab'
[1,1] = 'multi'

There are no test images that can be used for pseudo-labeling
'''

#%% Label processing
# Create the desired labels
processed_labels = pd.DataFrame({ "image_id": train_labels["image_id"]
                                , "rust"    : train_labels.eval("rust == 1 or multiple_diseases == 1").astype(int)
                                , "scab"    : train_labels.eval("scab == 1 or multiple_diseases == 1").astype(int)
                                }
                               )

# Write the labels
processed_labels.to_csv("data/processed_labels.csv")

#%% Image EDA
# Load an image
img = Image.open("data/plant-pathology-2020-fgvc7/images/Test_1.jpg")

# Display the image
img.show()

'''
Images are RGB, high resolution
To reduce image size, we can attempt to remove background or otherwise identify ROI
'''

#%% Background removal
# Function based on: https://www.kaggle.com/tarunpaparaju/plant-pathology-2020-eda-models
def load_image(image_path):
    image = cv2.imread(image_path)
    return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

def edge_and_cut(img, show_result=False):
    emb_img = img.copy()
    edges = cv2.Canny(img, 100, 200)
    edge_coors = []
    for i in range(edges.shape[0]):
        for j in range(edges.shape[1]):
            if edges[i][j] != 0:
                edge_coors.append((i, j))

    row_min = edge_coors[np.argsort([coor[0] for coor in edge_coors])[0]][0]
    row_max = edge_coors[np.argsort([coor[0] for coor in edge_coors])[-1]][0]
    col_min = edge_coors[np.argsort([coor[1] for coor in edge_coors])[0]][1]
    col_max = edge_coors[np.argsort([coor[1] for coor in edge_coors])[-1]][1]
    new_img = img[row_min:row_max, col_min:col_max]

    if show_result:
        emb_img[row_min - 10:row_min + 10, col_min:col_max] = [255, 0, 0]
        emb_img[row_max - 10:row_max + 10, col_min:col_max] = [255, 0, 0]
        emb_img[row_min:row_max, col_min - 10:col_min + 10] = [255, 0, 0]
        emb_img[row_min:row_max, col_max - 10:col_max + 10] = [255, 0, 0]

        fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(30, 20))
        ax[0].imshow(img, cmap='gray')
        ax[0].set_title('Original Image', fontsize=24)
        ax[1].imshow(edges, cmap='gray')
        ax[1].set_title('Canny Edges', fontsize=24)
        ax[2].imshow(emb_img, cmap='gray')
        ax[2].set_title('Bounding Box', fontsize=24)

        plt.show()

    return new_img

#%% Image cropping
for image_id in train_labels["image_id"]:
    cropped_img = edge_and_cut( load_image(f"data/plant-pathology-2020-fgvc7/images/{image_id}.jpg")
                              , show_result=False
                              )

    cv2.imwrite(f"data/processed images/{image_id}.png", cropped_img)

