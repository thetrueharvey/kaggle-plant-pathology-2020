'''
Training of a CNN model using PyTorch Lightning with Pseudolabeled dataset
'''
# Access tensorboard
#tensorboard --logdir lightning_logs

# Experiments


#%% Setup
import numpy  as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt

from PIL import Image

import torch
import torch.nn.functional as F

from torchvision      import transforms
from torch.utils.data import DataLoader

from torchtools.optim import RangerLars

import pytorch_lightning as pl

import _transforms  as T

from callbacks import TransformAdder

from topologies.SEResNeXt101 import SeResNeXt101
from topologies.MobileNetV2  import MobileNetV2
from topologies.Densenet     import Densenet201

#%% Data
# Load the labels
labels = pd.read_csv("data/processed_labels.csv")

# Train-test split
train_test = train_test_split( labels["image_id"]
                             , labels[["rust", "scab"]]
                             , random_state=42
                             , test_size   =0.2
                             )

X_train, X_test, y_train, y_test = train_test

pseudo_labels = pd.read_csv("data/pseudo_processed_labels.csv")

X_pseudo, y_pseudo = labels["image_id"].to_list(), labels[["rust", "scab"]].to_list()

X_train = X_train + X_pseudo
y_train = y_train + y_pseudo

#%% Dataset
class PlantDataset:
    def __init__(self, X, y, transforms):
        self.X = X.to_numpy()
        self.y = y.to_numpy()

        self.transforms = transforms

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        # Fetch the image
        image_id = self.X[idx]

        # Load the image
        img = Image.open(f"data/processed images/{image_id}.jpg").convert('RGB')

        # Format the label
        label = self.y[idx]

        # Apply transformations
        img_ = self.transforms(img)

        return img_, label.astype(np.float)

#%% Transforms
# Aux transform
aux_transforms = [ #T.RandomDistortion()
                   T.RandomFlip()
                 , T.RandomColourJitter()
                 , T.RandomErasures(n_erasures=[1, 3], h=[0.05, 0.2], w=[0.05, 0.2])
                 , T.RandomAffine(translate=([-0.2,0.2], [-0.2,0.2]), angle=[-20, 20],   scale=[-0.2,0.2],      shear=([-15,15], [-15,15]))
                 , T.RandomNoise()
                 , T.RandomCrop()
                 #, T.Skew()
                 ]

# Base transform
base_transforms = [ transforms.Resize((400, 400))
                  , transforms.ToTensor()
                  #, transforms.Normalize(mean=[33.89 / 255.0], std=[70.87 / 255.0])
                  ]



#%% Loaders
train_loader = DataLoader( dataset    =PlantDataset(X_train, y_train, transforms.Compose([T.ChooseOne(aux_transforms)] + base_transforms))
                         , batch_size =8
                         , shuffle    =True
                         , pin_memory =True
                         , num_workers=8
                         )

val_loader = DataLoader( dataset   =PlantDataset(X_test, y_test, transforms.Compose(base_transforms))
                       , batch_size=32
                       , pin_memory=True
                       , num_workers=8
                       )

transform_adder = TransformAdder( base_transforms=base_transforms
                                , aux_transforms =aux_transforms
                                , dataloader     =train_loader
                                )

# %% Lightning model
class LightningModel(pl.LightningModule):
    def __init__(self, model):
        super(LightningModel, self).__init__()

        # Instantiate the model
        self.model = model(num_classes=2)

    def forward(self, x):
        return self.model(x)

    def dual_bce_loss(self, logits, labels):
        rust_loss = F.binary_cross_entropy_with_logits(logits[:,0], labels[:,0])
        scab_loss = F.binary_cross_entropy_with_logits(logits[:,1], labels[:,1])

        return rust_loss + scab_loss

    def training_step(self, batch, batch_idx):
        X,y = batch
        y_   = self.forward(X)
        loss = self.dual_bce_loss(y_, y)

        # Predictions
        p = (torch.sigmoid(y_) > 0.5).int()

        # Calculate the associated indices for each class
        indices = {key: torch.eq(y[:, 0], a) & torch.eq(y[:, 1], b)
                   for key, [a, b] in zip(["healthy", "rust", "scab", "multi"]
                                          , [[0, 0], [1, 0], [0, 1], [1, 1]]
                                          )
                   }

        # Hence calculate accuracies
        accuracies = {key: (torch.eq(p[indices_, 0], y[indices_, 0]) & torch.eq(p[indices_, 1], y[indices_, 1])).float()
                      for key, indices_ in indices.items()
                      }

        total_acc = torch.cat([t for _, t in accuracies.items()])

        return { "loss"   : loss
               , "healthy_acc": accuracies["healthy"].mean()
               , "rust_acc"   : accuracies["rust"].mean()
               , "scab_acc"   : accuracies["scab"].mean()
               , "multi_acc"  : accuracies["multi"].mean()
               , "total_acc"  : total_acc.mean()
               , "log" : { "train_loss"   : loss
                         , "healthy_acc": accuracies["healthy"].mean()
                         , "rust_acc"   : accuracies["rust"].mean()
                         , "scab_acc"   : accuracies["scab"].mean()
                         , "multi_acc"  : accuracies["multi"].mean()
                         , "total_acc"  : total_acc.mean()
                         }
               }

    def validation_step(self, batch, batch_idx):
        X, y = batch
        y_ = self.forward(X)
        loss = self.dual_bce_loss(y_, y)

        # Predictions
        p = (torch.sigmoid(y_) > 0.5).int()

        # Calculate the associated indices for each class
        indices = {key:torch.eq(y[:,0], a) & torch.eq(y[:,1], b)
                   for key,[a,b] in zip( ["healthy", "rust", "scab", "multi"]
                                       , [[0,0],[1,0],[0,1],[1,1]]
                                       )
                  }

        # Hence calculate accuracies
        accuracies = { key:(torch.eq(p[indices_,0], y[indices_,0]) & torch.eq(p[indices_,1], y[indices_,1])).float()
                       for key,indices_ in indices.items()
                     }

        total_acc = torch.cat([t for _,t in accuracies.items()])

        return { "val_loss"   : loss
               , "val_healthy_acc": accuracies["healthy"]
               , "val_rust_acc"   : accuracies["rust"]
               , "val_scab_acc"   : accuracies["scab"]
               , "val_multi_acc"  : accuracies["multi"]
               , "val_total_acc"  : total_acc
               , "log" : { "val_loss"   : loss
                         , "val_healthy_acc": accuracies["healthy"].mean()
                         , "val_rust_acc"   : accuracies["rust"].mean()
                         , "val_scab_acc"   : accuracies["scab"].mean()
                         , "val_multi_acc"  : accuracies["multi"].mean()
                         , "val_total_acc"  : total_acc.mean()
                         }
               }

    def validation_end(self, outputs):
        # Validation set loss
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()

        # Accuracies
        total_acc = torch.cat([x['val_total_acc'] for x in outputs]).mean()

        tensorboard_logs = { "val_loss"     : avg_loss
                           , "val_total_acc": total_acc
                           }
        return { "avg_val_loss" : avg_loss
               , "val_total_acc": total_acc
               , "log"         : tensorboard_logs
               }

    def configure_optimizers(self):
        return self.optimizer

    def train_dataloader(self):
        return train_loader

    def val_dataloader(self):
        return val_loader

    def configure_optimizers(self):
        optimizer = RangerLars(self.parameters())
        return optimizer

    def visualize_batches(self, n=1, rows=4, cols=4):
        # Get a batch of images
        i = 1
        for X,y in train_loader:
            result = torch.cat( [torch.cat([X[i_col + i_row] for i_row in range(rows)], dim=-1)
                                 for i_col in [i * rows for i in range(cols)]
                                ]
                              , dim=-2
                              )

            img = transforms.functional.to_pil_image(result)
            img.show()

            if i == n:
                break

            i += 1

#%% Model Initialization
if __name__ == '__main__':
    # Create a model
    model = LightningModel(model=Densenet201)

    # Visualize a batch
    #model.visualize_batches()

    # %% Training
    trainer = pl.Trainer( gpus     =1
                        , precision=32
                        , progress_bar_refresh_rate=1
                        , accumulate_grad_batches  =8
                        #, callbacks=[transform_adder]
                        #, fast_dev_run=True
                        #, reload_dataloaders_every_epoch=True
                        #, resume_from_checkpoint="lightning_logs/version_0/checkpoints/epoch=68.ckpt"
                        )
    trainer.fit(model)
    a=0