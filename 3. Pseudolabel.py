'''
Pseudo-labeling to increase available training data
Validation will only be done on the original validation set from the labeled training data
'''
#%% Setup
import pandas as pd
import torch

from torchvision      import transforms
from torch.utils.data import DataLoader


from topologies.SEResNeXt101 import SeResNeXt101
from topologies.MobileNetV2  import MobileNetV2
from topologies.Densenet     import Densenet201

#%% Dataset
# Load the labels
labels = pd.read_csv("data/processed_test_labels.csv")

X_pseudo, y_pseudo = labels["image_id"].to_list(), labels[["rust", "scab"]].to_list()

# Base transform
base_transforms = [ transforms.Resize((400, 400))
                  , transforms.ToTensor()
                  #, transforms.Normalize(mean=[33.89 / 255.0], std=[70.87 / 255.0])
                  ]

pseudo_loader = DataLoader( dataset    =PlantDataset(X_pseudo, y_pseudo, transforms.Compose(base_transforms))
                          , batch_size =32
                          , pin_memory =True
                          , num_workers=8
                          )

#%% Model
model = LightningModel.load_from_checkpoint(path, model=Densenet201)
model.eval()

#%% Labeling
pseudo_labels = torch.concat([(torch.sigmoid(model(x)) > 0.5).int() for x in pseudo_loader])

pseudo_labels = pd.DataFrame({ "image_id":X_pseudo
                             , "rust"    :pseudo_labels[0]
                             , "scab"    :pseudo_labels[1]
                             }
                            )

