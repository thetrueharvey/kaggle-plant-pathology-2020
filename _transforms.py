"""
Transforms for classification problems (i.e. image-only)
These are transforms that are more complex than the standard TorchVision transforms
"""
#%% Setup
import random
import PIL
import torch
import Augmentor

from torchvision import transforms

#%% TODO
# Add perspective transforms (need to check how implementation works)

#%% Transforms
# Random Noise
class RandomNoise:
    def __init__( self
                , frequency : list  = [0.05,0.3]
                , start_prob: float = 0.3
                , stop_prob : float = 0.7
                , step_prob : float = 3e-5
                ):
        """
        Adds noise (random values) to the pixels of the image
        :param frequency: Baseline range of probability for noise
        :param decay    : The rate at which the initial frequency range decays, per image
        """
        # Class attributes
        self.frequency  = frequency
        self.stop_prob  = stop_prob
        self.step_prob  = step_prob

        # Initialize the probability
        self.probability = start_prob

    def __call__(self, img: torch.Tensor):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Calculate the frequency
        frequency = random.uniform(self.frequency[0], self.frequency[1])

        # Type conversion if necessary
        if type(img) is not torch.Tensor:
            img = transforms.functional.to_tensor(img)

        # Generate random noise
        noise = torch.where(torch.rand_like(img) < frequency, (torch.rand_like(img) * 2) - 1, torch.zeros_like(img))

        # Apply the noise and clamp the values
        img = (img + noise).clamp(0,1)

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return transforms.functional.to_pil_image(img)

class RandomErasures:
    def __init__( self
                , n_erasures: list  = [2,3]
                , h         : list  = [0.02, 0.1]
                , w         : list  = [0.02, 0.1]
                , fill      : list  = [0,1]
                , start_prob: float = 0.3
                , stop_prob : float = 0.7
                , step_prob : float = 3e-5
                ):
        """
        Extension of the normal TorchVision RandomErase transformation, with the capability to implement multiple erasures, and anneal the transformation
        TODO: Documentation
        :param n_erasures:
        :param h:
        :param w:
        :param fill:
        :param n_decay:
        :param size_decay:
        """
        # Class attributes
        self.n_erasures  = n_erasures
        self.h           = h
        self.w           = w
        self.fill        = fill
        self.stop_prob = stop_prob
        self.step_prob = step_prob

        # Initialize the probability
        self.probability = start_prob

    def __call__(self, img: torch.Tensor):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Calculate the new number of erasures
        n_erasures = int(random.uniform(self.n_erasures[0], self.n_erasures[1]))

        # Type conversion if necessary
        if type(img) is not torch.Tensor:
            img = transforms.functional.to_tensor(img)

        # Add the erasures
        for _ in range(n_erasures):
            # Calculate the new sizes
            h = random.uniform(self.h[0], self.h[1])
            w = random.uniform(self.w[0], self.w[1])

            # Calculate the height and width
            h_ = int(h * img.shape[1])
            w_ = int(w * img.shape[2])

            # Calculate the top corner of the erasure
            x = random.randint(0, img.shape[2] - w_)
            y = random.randint(0, img.shape[1] - h_)

            # Calculate a random fill
            fill = random.uniform(self.fill[0], self.fill[1])

            # Fill the section
            img[:, y:(y + h_), x:(x + w_)] = fill

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return transforms.functional.to_pil_image(img)

class RandomAffine:
    def __init__( self
                , n             : int   = 1
                , angle         : list  = [-15, 15]
                , translate     : tuple = ([-0.15, 0.15], [-0.15, 0.15])
                , scale         : list  = [-0.2, 0.2]
                , shear         : tuple = ([-10, 10], [-10, 10])
                , start_prob: float = 0.3
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):
        """
        Extension of the normal TorchVision RandomAffine transformation, with the capability to anneal the transformation parameters
        TODO: Documentation
        :param n:
        :param angle:
        :param translate:
        :param scale:
        :param shear:
        :param decay:
        """
        # Class attributes
        self.n             = n
        self.angle         = angle
        self.translate     = translate
        self.scale         = scale
        self.shear         = shear
        self.stop_prob     = stop_prob
        self.step_prob     = step_prob

        # Initialize the probability
        self.probability = start_prob

    def __call__(self, img: torch.Tensor):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        for _ in range(self.n):
            # Calculate each parameter and check if it should be applied
            angle = 0
            if random.randrange(0, 1) < self.probability:
                angle = random.uniform(self.angle[0], self.angle[1])

            translate_x = 0
            if random.randrange(0, 1) < self.probability:
                translate_x = random.uniform(self.translate[0][0], self.translate[0][1])

            translate_y = 0
            if random.randrange(0, 1) < self.probability:
                translate_y = random.uniform(self.translate[1][0], self.translate[1][1])

            scale = 1
            if random.randrange(0, 1) < self.probability:
                scale = 1 + random.uniform(self.scale[0], self.scale[1])

            shear_x = 0
            if random.randrange(0, 1) < self.probability:
                shear_x = random.uniform(self.shear[0][0], self.shear[0][1])

            shear_y = 0
            if random.randrange(0, 1) < self.probability:
                shear_y = random.uniform(self.shear[1][0], self.shear[1][1])

            # Apply the affine transformation
            img = transforms.functional.affine( img      =img
                                              , angle    =angle
                                              , translate=[translate_x, translate_y]
                                              , scale    =scale
                                              , shear    =[shear_x, shear_y]
                                              , fillcolor=random.randrange(0, 1)
                                              )

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img

class RandomFlip:
    def __init__(self
                , start_prob: float = 0.3
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):
        """
        Flip an image across either axis
        TODO: Documentation
        """
        # Class attributes
        self.stop_prob     = stop_prob
        self.step_prob     = step_prob

        # Initialize the probability
        self.probability = start_prob

    def __call__(self, img: torch.Tensor):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Calculate each parameter and check if it should be applied
        if random.randrange(0, 1) < self.probability:
            # Apply the affine transformation
            img = transforms.functional.hflip(img=img)

        if random.randrange(0, 1) < self.probability:
            # Apply the affine transformation
            img = transforms.functional.vflip(img=img)

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img

class RandomCrop:
    def __init__( self
                , crop_range : list  = ([0.05, 0.15], [0.05, 0.15])
                , start_prob: float = 0.3
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):

        # Class attributes
        self.crop_range  = crop_range
        self.stop_prob = stop_prob
        self.step_prob = step_prob

        # Initialize the probability
        self.probability = start_prob

    def __call__(self, img):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Calculate the crop proportions
        crop_x = 1 - random.uniform(self.crop_range[0][0], self.crop_range[0][1])
        crop_y = 1 - random.uniform(self.crop_range[1][0], self.crop_range[1][1])

        # Hence calculate the absolute dimensions
        w = int(crop_x * img.width)
        h = int(crop_y * img.height)

        # Calculate the top corner of the crop
        x = random.randint(0, img.width - w)
        y = random.randint(0, img.height - h)

        # Apply the transformation
        img = transforms.functional.resized_crop( img
                                                , y
                                                , x
                                                , h
                                                , w
                                                , [ img.height, img.width]
                                                )

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img

class RandomColourJitter:
    def __init__( self
                , brightness   : list  = [-0.4, 0.4]
                , contrast     : list  = [-0.4, 0.4]
                , gamma        : list  = [-0.4, 0.4]
                , hue          : list  = [-0.4, 0.4]
                , saturation   : list  = [-1, 2]
                , start_prob: float = 0.3
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):

        # Class attributes
        self.brightness = brightness
        self.contrast   = contrast
        self.gamma      = gamma
        self.hue        = hue
        self.saturation = saturation
        self.stop_prob  = stop_prob
        self.step_prob  = step_prob

        # Initialize the probability
        self.probability = start_prob

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Randomize and apply each transformation if necessary
        if random.randrange(0, 1) < self.probability:
            brightness = 1 + random.uniform(self.brightness[0], self.brightness[1])
            img = transforms.functional.adjust_brightness( img=img
                                                         , brightness_factor=brightness
                                                         )

        if random.randrange(0, 1) < self.probability:
            contrast = 1 + random.uniform(self.contrast[0], self.contrast[1])
            img = transforms.functional.adjust_contrast( img            =img
                                                       , contrast_factor=contrast
                                                       )

        if random.randrange(0, 1) < self.probability:
            gamma = 1 + random.uniform(self.gamma[0], self.gamma[1])
            img = transforms.functional.adjust_gamma( img  =img
                                                    , gamma=gamma
                                                    )

        if random.randrange(0, 1) < self.probability:
            hue = random.uniform(self.hue[0], self.hue[1])
            img = transforms.functional.adjust_hue( img       =img
                                                  , hue_factor=hue
                                                  )

        if random.randrange(0, 1) < self.probability:
            saturation = 1 + random.uniform(self.saturation[0], self.saturation[1])
            img = transforms.functional.adjust_saturation( img              =img
                                                         , saturation_factor=saturation
                                                         )

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img

class Skew:
    def __init__( self
                , start_prob: float = 0.2
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):
        # Class attributes
        self.stop_prob = stop_prob
        self.step_prob = step_prob

        # Initialize the probability
        self.probability = start_prob

        # Create the skew transform
        p = Augmentor.Pipeline()
        p.skew_tilt(probability=1.0)

        # Convert to a Torchvision transform
        self.skew = p.torch_transform()

    def __call__(self, img):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Apply the transformation
        img = self.skew(img)

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img


class RandomDistortion:
    def __init__( self
                , grid       =[5,5]
                , magnitude  =8
                , start_prob: float = 0.2
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):
        # Class attributes
        self.stop_prob = stop_prob
        self.step_prob = step_prob

        # Initialize the probability
        self.probability = start_prob

        # Create the skew transform
        p = Augmentor.Pipeline()
        p.random_distortion( probability=1.0
                           , grid_width =grid[0]
                           , grid_height=grid[1]
                           , magnitude  =magnitude
                           )

        # Convert to a Torchvision transform
        self.random_distortion = p.torch_transform()

    def __call__(self, img):
        # Check if the transformation should be applied at all
        if random.randrange(0, 1) > self.probability:
            return img

        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Apply the transformation
        img = self.random_distortion(img)

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img


class GaussianDistortion:
    def __init__( self
                , grid       =[6,6]
                , magnitude  =6
                , start_prob: float = 0.2
                , stop_prob: float = 0.7
                , step_prob: float = 3e-5
                ):
        # Class attributes
        self.stop_prob = stop_prob
        self.step_prob = step_prob

        # Initialize the probability
        self.probability = start_prob

        # Create the skew transform
        p = Augmentor.Pipeline()
        p.gaussian_distortion( probability=1.0
                             , grid_width =grid[0]
                             , grid_height=grid[1]
                             , magnitude  =magnitude
                             , corner     ="bell"
                             , method     ="in"
                             )

        # Convert to a Torchvision transform
        self.gaussian_distortion = p.torch_transform()

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is torch.Tensor:
            img = transforms.functional.to_pil_image(img)

        # Apply the transformation
        img = self.gaussian_distortion(img)

        # Update the probability
        if self.probability < self.stop_prob:
            self.probability += self.step_prob

        return img

class StepFilter:
    def __init__(self, cutoff = 0.7):
        self.cutoff = cutoff

    def __call__(self, img):
        # Type conversion if necessary
        if type(img) is not torch.Tensor:
            img = transforms.functional.to_tensor(img)

        # Apply the transformation
        img[img > self.cutoff]  = 1
        img[img <= self.cutoff] = 0

        return img



#%% Collation transforms
# Random Order
class RandomOrder:
    def __init__(self, transforms):
        """
        Applies the given transforms in a random order
        :param transforms:
        :param probability: float. The probability of any transform being used
        """
        self.transforms  = transforms

    def __call__(self, img):
        # Shuffle transforms
        random.shuffle(self.transforms)

        # Apply each transform
        for transform in self.transforms:
            img = transform(img)

        return img

class ChooseOne:
    def __init__(self, transforms):
        """
        Applies one of the provided transforms
        :param transforms:
        """
        self.transforms = transforms

    def __call__(self, img):
        # Shuffle transforms
        random.shuffle(self.transforms)

        # Apply a random transform
        img = self.transforms[0](img)

        return img



#%% Utility functions
def relu(x, stop=0.25):
    """
    Forces a result to 0 if it is negative
    :param x:
    :return:
    """
    return stop if x < stop else x