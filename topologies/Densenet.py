'''
Wrapper around the torchvision implementation of Densenet for rapid training
'''
#%% Setup
import torch.nn as nn
from torchvision.models.densenet import densenet201

#%% Model class
class Densenet201(nn.Module):
    def __init__(self, num_classes=2):
        super(Densenet201, self).__init__()

        # Get the model
        self.model = densenet201(pretrained=True)

        # Update the head
        self.model.classifier = nn.Linear(1920, num_classes)

    def forward(self, x):
        return self.model(x)
