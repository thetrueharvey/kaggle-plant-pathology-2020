"""
Custom Callbacks
"""
#%% Setup
from torchvision                      import transforms
from pytorch_lightning.callbacks.base import Callback

#%% Transform adder
class TransformAdder(Callback):
    def __init__( self
                , base_transforms
                , aux_transforms
                , dataloader
                , delay           =1
                , patience        =50
                , cooldown        =20
                ):
        # Class attributes
        self.base_transforms = base_transforms
        self.aux_transforms  = aux_transforms
        self.dataloader      = dataloader
        self.delay           = delay
        self.patience        = patience
        self.cooldown        = cooldown
        self.add_transforms  = []

        # Initialize counters
        self.step             = 0
        self.best_loss        = 1e6
        self.steps_since_best = 0
        self.steps_since_add  = 0

    def on_validation_end(self, trainer, pl_module):
        loss = trainer.callback_metrics["avg_val_loss"]
        self.step += 1
        self.steps_since_add += 1

        if loss > self.best_loss:
            self.steps_since_best += 1

        if loss < self.best_loss:
            self.best_loss = loss
            self.steps_since_best = 0

        if self.step < self.delay or self.steps_since_best < self.patience or len(self.aux_transforms) == 0 or self.steps_since_add < self.cooldown:
            return

        if self.steps_since_best >= self.patience:
            add_transform = self.aux_transforms[0]
            self.aux_transforms = self.aux_transforms[1:]
            self.add_transforms = self.add_transforms + [add_transform]

            new_transforms = transforms.Compose(self.add_transforms + self.base_transforms)

            self.dataloader.dataset.transforms = new_transforms

            self.steps_since_add = 0

            self.best_loss = 1e6
